<?php
/**
 * Template Name: New CIM Thank You Page Style
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Step up to the plate</title>
	<style>.async-hide { opacity: 0 !important} </style>
	<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
	h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
	(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
	})(window,document.documentElement,'async-hide','dataLayer',4000,
	{'GTM-PH2XSKQ':true});</script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TDZV4H');</script>
	<!-- End Google Tag Manager -->

<!-- Oracle Maxymiser Script Start -->
<script type="text/javascript" src="//service.maxymiser.net/api/us/keypathedu.com/651afc/mmapi.js"></script>
<!-- Oracle Maxymiser Script End -->

	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory');?>/dist/css/styles.css">

	<!-- <link rel="stylesheet" type="text/css" href="dist/css/stylesLandingV1.css"> -->
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/favicon-16x16.png">
	<link rel="mask-icon" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
</head>
	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TDZV4H"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
	<div id="LandingPage" class="CIM">

	<div class="main-header LanndingPage">
	<div class="row">
		<div class="main-header__container">
			<div class="main-header__search-bar">
				<input type="text" class="main-header__search-input" placeholder="Search">
				<div class="main-header__search-input-close"></div>
			</div>
			<div class="main-header__left-nav">
				<a href="/" class="main-header__logo">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Landing page/CIM Logo@2x.png" alt="" class="main-header__logo-img-red">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Landing page/CIM Logo@2x.png" alt="" class="main-header__logo-img-white">
				</a>
			</div>
			<div class="main-header__right-nav">
				<a class="main-header__right-btn main-header__call-btn" href="tel:810-766-2167"></a>
				<a class="main-header__call-text" href="tel:810-766-2167">810-766-2167</a>
			</div>
		</div>
	</div>
</div>

	<div class="LandingPage-fullwidth-video-cta" style="background-image: url('<?php echo get_bloginfo('template_directory');?>/assets/images/Landing page/shutterstock_564107569@2x.jpg');">
	<div class="row">
		<div class="LandingPage-fullwidth-video-cta__wrap">
			<h1 class="LandingPage-fullwidth-video-cta__title">Thank you!</h1>
			<div class="LandingPage-fullwidth-video-cta__sub-text">
				<p>We&rsquo;ve sent a confirmation message to the email address you provided. One of our campus representatives should be contacting you shortly.</p>
			</div>
		</div>
	</div>
</div>

<div class="contact">
<div class="row">
	<div class="contact-form-head" id="cform">
		<div class="layout-sidebar-right__left-side">
			<div class="cim-text-box">
			<div class="row">
			<p>In the meantime, here are some things that might be helpful to you.</p>
			</div>
			</div>
			<div class="block-grid-links block-grid block-grid--bg-white block-grid--2col block-grid--slim-top-padding">
			<div class="row">
			<div class="block-grid__grid">
			<div class="block-grid__grid-item">
			<a target="_blank" href="https://www.baker.edu/request-info/request-info-thank-you/download-the-new-2017-2018-catalog" class="content-card content-card--hover-effect ">
			<div class="content-card__photo" style="background-image: url('https://www.baker.edu/assets/images/catalog-thumbnail.jpg');"></div>
			<div class="content-card__text-wrap content-card_text-wrap--desktop-gutter">
			<div class="content-card__title">Download the new 2017-2018 catalog</div>
			<div class="content-card__text">
			<p>Learn more about Baker College, our campuses, the academic programs we offer, our admission requirements, and more.</p>
			</div>
			<span href="request-info/request-info-thank-you/download-the-new-2017-2018-catalog" class="read-more content-card__read-more ">Download The Baker College Catalog</span>
			</div>
			</a>
			</div>
			</div>
			</div>
			</div>
		</div>

	</div>
	<div class="sidebar">
		<div class="sidebar__locations">
			<div class="sidebar__locations--Address">
				<h4>CIM-Muskegon</h4>
				<p>1903 Marquette Ave</p>
				<p>Muskegon, MI 49442</p>
				<a href="https://www.google.com/maps/place/1903+Marquette+Ave,+Muskegon,+MI+49442/@43.2443111,-86.1987796,17z/data=!3m1!4b1!4m5!3m4!1s0x88197ceb23c7c5ff:0x2bbf2293dc2c7892!8m2!3d43.2443111!4d-86.1965856" target="_blank">Get Directions</a>
			</div>
			<div class="sidebar__locations--Address">
				<h4>CIM-Port Huron</h4>
				<p>3402 Lapeer Road</p>
				<p>Port Huron, MI 48060</p>
				<a href="https://www.google.com/maps/place/3402+Lapeer+Rd,+Port+Huron,+MI+48060/@42.9791066,-82.4702114,17z/data=!3m1!4b1!4m5!3m4!1s0x88259c26a075a79f:0x75bc71d307dc6855!8m2!3d42.9791066!4d-82.4680174" target="_blank">Get Directions</a>
			</div>

		</div>

		<div class="sidebar__QuickLinks">
			<h4>Quick Links</h4>
			<ul class="sidebar__QuickLinks--list">
				<li><a href="https://www.baker.edu/academics/affiliated-institutions/culinary-institute-of-michigan/programs/culinary-arts/baking-and-pastry-associate-of-applied-science" target="_blank" class="sidebar__QuickLinks--link">Baking & Pastry</a></li>
				<li><a href="https://www.baker.edu/academics/affiliated-institutions/culinary-institute-of-michigan/programs/culinary-arts/culinary-arts" target="_blank" class="sidebar__QuickLinks--link">Cuilinary Arts</a></li>
				<li><a href="https://www.baker.edu/academics/affiliated-institutions/culinary-institute-of-michigan/programs/business-management/food-and-beverage-management-associate-of-applied-science" target="_blank" class="sidebar__QuickLinks--link">Food & Bevrage Management</a></li>
			</ul>
		</div>
	</div>
</div>
</div>





		<footer class="footer-section">
	<div class="footer-section__bottom row">
		<div class="footer-section__logo">
				<img class="logo-icon" src="http://baker.dev.trafficdesk.io/assets/images/logo/new-logo--red-white.svg"/>
		</div>
		<p class="footer-section__accredited">Accredited by <span class="footer-section__link_underline"><a href="#">The Higher Learning Commission</a></span>. An  equal opportunity affirmative action institution.
		An approved institution of the <span class="footer-section__link_underline"><a href="#">National Council for State Authorization Reciprocity Agreements (NC-SARA)</a></span>
		and the <span class="footer-section__link_underline"><a href="#">Midwestern Higher Education Compact (MHEC)</a></span>
		</p>
		<div class="footer-section__bottom_rights-reserved"><a href="http://www.baker.edu">Visit baker.edu</a>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;© 2017 All rights reserved. Baker College</div>
	</div>
</footer>

		<div class="fade-screen"></div>
		</div>
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5890dcbf83803e36"></script>
		<script src="<?php echo get_bloginfo('template_directory');?>/dist/js/main.js" type="text/javascript" /></script>
		<script src="https://use.typekit.net/jyv0hxv.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>

		</body>
</html>
