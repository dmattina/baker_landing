
$('input').jvFloat();

$(document).ready(function(){
  /* Catch Query strings and pass the same to Home page Icons link - Start */
  var queryParameters = window.location.href.split("?")[1];
  if(queryParameters != null){
    $(".gen_icons a").each(function(){
      var temphref = $(this).attr("href");
      temphref += "?";
      temphref += queryParameters;
      $(this).attr("href", temphref);
    });
  }
  /* Catch Query strings and pass the same to Home page Icons link - END */

  $('.Thank .head').css('height', $(window).height() + "px");
  console.log('height added' + $('.Thank .head').height());
  
  //$('#SlideInfo h1, .success_holder h2').append('<i> > </i>');
   $('.all_programs > h1').append(' <i>  </i>');

  // All Program in single wysiwig editor - Toggle 
  $('.all_programs > h1').click(function(e){
      var current = $(this).next('blockquote');
      $('.all_programs > blockquote').not(current).hide(100);
      $('.all_programs > h1 i').not($(this).find('i')).removeClass('open_arrow');
      $('.all_programs > h1').not($(this)).removeClass('open'); // open = Mobile  || open_arrow = desktop

      current.slideToggle();
      // open = Mobile  || open_arrow = desktop
      if($(this).find('i').hasClass('open_arrow') || $(this).hasClass('open') ){
          $(this).find('i').removeClass('open_arrow');
          $(this).removeClass('open');
      }
      else if ( ! $(this).find('i').hasClass('open_arrow') || ! $(this).hasClass('open') ) {
          $(this).find('i').addClass('open_arrow');
          $(this).addClass('open')
      }
      if($(window).width() < 767){
                  
      }
  });


  // Separate Program - Toggle Function
  $('.hold > h3').click(function(e){
      var current = $(this).next('section');
      $('.hold > section').not(current).hide(100);
      $('.hold > h3 i').not($(this).find('i')).removeClass('open_arrow');
      current.slideToggle();
      if($(this).find('i').hasClass('open_arrow')){
          $(this).find('i').removeClass('open_arrow');
      }
      else if ( ! $(this).find('i').hasClass('open_arrow')) {
          $(this).find('i').addClass('open_arrow');
      }
  });

  $('.locations_holder .content .title > h3').click(function(e){
    if($(window).width() < 768){
      $('.locations_list, .additional_locations').toggle();
      $(this).find('i').toggleClass('loc_open_arrow');
    }
  })
  // Navigation adding AB test start
  $( "body.nav_ab #logo img" ).wrap( "<a href='/'></a>" );
  $('.sub-menu').hide();
  $('#top-menu li.menu-item-has-children > a').click(function(e) {
    e.preventDefault();
    if ($(this).siblings('.sub-menu').is(':visible')){
      //alert('f');
      $(this).removeClass('on')
      $(this).siblings('.sub-menu').hide();
    }
    else if ($(this).siblings('.sub-menu').is(':hidden')){
      //alert('h');
      $(this).addClass('on')
      $(this).siblings('.sub-menu').show();
    }
    
  });
  
  if ($(window).width() < 767){
      //$('.custom_nav').hide();
      $('.mob_menu').click(function(){
        if ($('.custom_nav').is(':visible')){
          $('.custom_nav').hide();
          $('#hamburger').removeClass('open');
          $('.nav_ab .mob_menu').removeClass('hamb_on');
          $('.hamb_text').text('Menu');
          MenuReset()
        }
        else if ($('.custom_nav').is(':hidden')){
          $('.custom_nav').show();
          $('#hamburger').addClass('open');
          $('.nav_ab .mob_menu').addClass('hamb_on');
          $('.hamb_text').text('Close');
        }
      });
  }
  
  function MenuReset(){
    $('#top-menu li.menu-item-has-children > a').removeClass('on');
    $('.sub-menu').hide();
  }
  // End AB test

  

});

/* 186284 - Nav Ab test Start */
function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}     
//-------------------------------
// Checking query string for PPC
var targetSource = getQueryVariable('menu');
if(targetSource == "show"){
          
    //store string in session if it not already
      var stringURL = sessionStorage.getItem('stringURL');
      sessionStorage.setItem('stringURL', 'show');
    }
    
    //retrieve stored string in session
    var stringURL = sessionStorage.getItem('stringURL');
    if(stringURL=='show'){
    $('body').addClass('nav_ab');
}
else{

}
// End HERE
/* 186284 - Nav Ab test End */
    

    //Trigger the event
    $(window).trigger('resize');


//Follow form Js
$(window).on('resize', function () {
    
    $('.Thank .head').css('height', $(this).height() + "px");
    formScroll();
});




function formScroll() {
  var fixmeTop = $('.fixme').offset().top;               // get initial position of the element
  var fixmePosition = $('.fixme').position();
  var leftmargin = $('.fixme').css('margin-left');
  //var width = document.getElementById('fix').offsetWidth;
  //var intoSize = $('.intro').height() + 35;
  var intoSize = $('#intro').height();
  $('.fixme').css({marginTop: '-' + intoSize + 'px',})

  var collapsePoint = $('.head').height() + 100    // Banner height + Form height
console.log(collapsePoint);

  //$(window).scroll(function() {   
  $(window).on('scroll touchmove', function(e) {                   // assign scroll event listener
        var currentScroll = $(window).scrollTop();        // get current position  
        var windowSize = $(window).width();
        var formHeight= $('.fixme').height();
        /*var tilesHeight= $('.tiles').offset().top ;
        var stopMe = tilesHeight - formHeight;*/
        var tilesHeight= $('.testimonial_holder').offset().top ;
        var stopMe = tilesHeight - formHeight;

       
        if (currentScroll >= (fixmeTop - 100) && windowSize > 1000) {    // apply position: fixed if you
          
            var fromTop = $('.fixme').offset().top;
            $('.fixme').css({                                 // scroll to that element or below it
          position: 'fixed',
          top: '80px',
          left: fixmePosition.left  + 'px',
          marginTop: '-' + intoSize + 'px',
          });
        } 
        else {                               // apply position: static
        $('.fixme').css({                      // if you scroll above it
          position:'relative',
          top:'auto',
          left :'auto',
            marginTop: '-' + intoSize + 'px',
          });
        }

        if (currentScroll >= (stopMe - 100) && windowSize > 1000) {           // apply position: fixed if you
              $('.fixme').css({                                 // scroll to that element or below it
                  position: 'absolute',
                  /*width: width + 'px',*/
                  top: stopMe + 'px',
                  left: fixmePosition.left + 'px',
                    marginTop: '-' + intoSize + 'px',
                });
        }
  });
//console.log (leftmargin);
}


$(document).ready(function(){
    
    formScroll();
    $('#Intro').click(function(){
      $('form.keypath').slideToggle();
      var formState = $("form.keypath");
      var windowSize = $(window).width();

    })

    if(isMobile.any()){
    var _originalSize = $(window).width() + $(window).height();
    var _originalHeadHeight = $(window).height();

    $(window).resize(function(){
        if($(window).width() + $(window).height() != _originalSize){
          console.log("keyboard on");
          //alert('keyboard on'); 
          //$(".head").css("height", _originalHeadHeight + "px");  
        }else{
          console.log("keyboard off");
          //alert('keyboard off');
        }
      });
    }
});





/* Mobile Keyboard On Detection for Avoid title text merging with request info form */

// Detect Mobile Device
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
}; 

$(document).ready(function(){
  
  

});


$.validator.addMethod('customphone', function (value, element) {
    //return this.optional(element) || /^\d{3}-\d{3}-\d{4}$/.test(value);
  var invalidPhoneNumbers = ["0000000000", "1111111111", "2222222222", "3333333333", "4444444444", "5555555555", "6666666666", "7777777777", "8888888888", "9999999999", "1234567890"];
  var valueRegex = value.replace(/[^0-9]/g,'');
  if (value.length > 0 && 10 != valueRegex.length) { return false; }
  return jQuery.inArray(valueRegex, invalidPhoneNumbers) == -1;
}, "Please enter a valid phone number");

$.validator.addMethod("fillProgram", function(value, element, param) {
  return this.optional(element) || value != param;
}, "Please specify the program you are interested in");

$.validator.addMethod("zipcode", function(value, element) {
  return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
}, "Please provide a valid zipcode.");

$.validator.addMethod("nonNumeric", function(value, element) {
    return this.optional(element) || !value.match(/[0-9]+/);
},"Only alphabatic characters allowed.");

$(document).ready(function() {
  // validate signup form on keyup and submit
  $(".keypath").validate({
    rules: {
      firstname: "required",
      lastname: "required",
      email: {
    required: true,
    email: true
    },
      dayphone: "customphone",
      city: {
    required: true,
    nonNumeric: true
    },
    city_temp: {
    required: true,
    nonNumeric: true
    },
      state: "required",
      state_temp: "required",
      zip: {
    required: true,
    zipcode: true,
    },
      gradyear: "required",
      LocationID: "required",
      CurriculumID: "required",
      areaofstudy: "required",
      LocationID2:"required",
    },
    messages: {
      firstname: "Please enter your First Name",
      lastname: "Please enter your Last Name",
      email: "Please enter a valid email address",
      dayphone: "Please enter a valid phone number",
      address: "Please enter your Address",
      city: "Please enter your City",
      state: "Please enter your State",
      city_temp: "Please enter your City",
      state_temp: "Please enter your State",
      zip: "Please enter your Zip Code",
      gradyear: "Please enter your Graduation Year",
      LocationID: "Please enter your Preferred campus location",
      LocationID2: "Please enter your Preferred campus location",
      CurriculumID: "Please specify the program you are interested in",
      areaofstudy: "Please select your area of study",
    }
  });
});

$(document).ready(function(){

  if(/android/i.test(navigator.userAgent.toLowerCase()) == true){
    $("#zip").inputmask('remove');
    $("#dayphone").inputmask('remove');
    $("#zip").attr('maxlength','5');      
    $("#dayphone, #evephone").attr('maxlength','10');
  }else{
    $(":input").inputmask();
  }

  $('#city_temp').hide();
  $('#state_temp').hide(); 
  $('.city_state_wrapper').hide();
 
  $("#zip").change(function() {
    /*if (jQuery.browser.msie && parseInt(jQuery.browser.version, 10) === 7)
      setLocation3(jQuery(this).val());
    else*/
    //Check if not empty zip value and length being 5 digits only by removing input mask ____ using Reqgular expression 
    var ziptemp = jQuery(this).val().replace(/^\_+|\_+$/gm,'');
    if(ziptemp.length == 5){
      setLocation4(ziptemp);
    }
  });

  function setLocation4(zip) {
      var url;
      if(window.location.pathname.indexOf("/dev/") == 0){
        url = window.location.protocol + "//" + window.location.host + "/dev/" + "wp-content/themes/bakerLanding_NEW/getZip2.php?zip=" + zip;
      }
      else{
        url = window.location.protocol + "//" + window.location.host + "/wp-content/themes/bakerLanding_NEW/getZip2.php?zip=" + zip;
      }
      $.ajax({
              async: false,
              dataType: "json",
              url: url,
              success: function(zip) {
                  try {
                      $("#city").val(zip[0][2]);
                      $("#state").val(zip[0][3]);
                      $("#city_temp").val(zip[0][2]);
                      $("#state_temp").val(zip[0][3]);
                  } catch (e) {
                      if($("#zip").val() !=''){
                        $('.city_state_wrapper').show();
                        $('#enterpriseform #city_temp').css('display','block');
                        $('#enterpriseform #state_temp').css('display','block');
                      }
                  }
              }
          })
          .fail(function() {
              $('.city_state_wrapper').show();
              $('#enterpriseform #city_temp').css('display','block');
              $('#enterpriseform #state_temp').css('display','block');
          });
  }

  $('#state_temp').change(function() {
    var state_s = $('#state_temp').val(); //.find(":selected").text();
    $("#state").val(state_s);
  });
  
  $("#city_temp").change(function() {
    $("#city").val($(this).val());
  });

  if (jQuery(window).width() < 768 ){
    jQuery('body').addClass('mobsticky');
      jQuery(window).scroll(function() {           
          var mainContent = jQuery('.success_holder').offset().top;
          var formBottom = mainContent - 22;
          console.log(formBottom);
          if (jQuery(this).scrollTop() >= formBottom ) { // this refers to window
              jQuery('body.mobsticky .sticky').show();
          }
          else if (jQuery(this).scrollTop() < formBottom ) { // this refers to window
              jQuery('body.mobsticky .sticky').hide();
          }       
      });
  }

  var stickyText = '<h6 class="sticky"><a href="#top" style="text-decoration:none">Request Info</a></h6>';
  jQuery('body').append(stickyText);
  jQuery(".sticky").on('click',function(){               
       jQuery('html, body').animate({
            scrollTop: jQuery(".form_holder").offset().top
      }, 200);              
  });

  /* a/b testing - 2 step form*/
  /* function getQueryVariable(variable)
  {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
      var pair = vars[i].split("=");
      if(pair[0] == variable){return pair[1];}
    }
    return(false);
  } 

  var targetSource = getQueryVariable('twostep');
  if(targetSource == "yes"){
      jQuery('body').addClass('twostep');
  }

  if(jQuery('body').hasClass('twostep')){
  */
    jQuery('form.keypath').hide();
    jQuery('#Intro > h3').text('Find Your Program'); 
    jQuery('h6.sticky a').text('Find Your Program'); 
    //jQuery('.step.step2').insertAfter('.step.current');
    jQuery('.step.step2').hide();
    jQuery('.step.current').show();
    jQuery('form.keypath').show();

    jQuery(this).find('.nextstep.button').click(function(){
      if(jQuery(this).closest('.keypath').valid()){
        jQuery(this).parents('.step').slideUp().next('.step').slideDown();
      }
      return false;
    });

    jQuery('.stepinfo.goback').click(function(){
      jQuery('.step.current').show();
      jQuery('.step.step2').hide();
    });
 /* } */
});
