<?php
/**
 * The template for displaying the header
 */
?>
<!DOCTYPE html>
<head>  

     <!-- Set the viewport width to device width for mobile -->
     <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Sass Generated File | Sass file /styles.sass
    <link href="mobile.css" rel="stylesheet" >-->
    <!-- Sass Generated File | Sass file /styles.sass -->

  <!--<link href="<?php echo get_bloginfo('template_directory');?>/css/style2.css" rel="stylesheet">
    <link href="<?php echo get_bloginfo('template_directory');?>/css/responsive2.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory');?>/css/animations.css?v=2016.08.29">
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory');?>/css/jvfloat.css?v=2016.08.29">-->
  
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/favicon-16x16.png">
<link rel="mask-icon" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
<!-- Oracle Maxymiser Script Start -->
<script type="text/javascript" src="//service.maxymiser.net/api/us/keypathedu.com/651afc/mmapi.js"></script>
<!-- Oracle Maxymiser Script End -->

 <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-5JDHH2');</script>
  <!-- End Google Tag Manager -->

  <?php wp_head(); ?>
</head>
<body class="<?php echo get_the_title();?>" >
<!-- Google Tag Manager -->
  <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5JDHH2"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager -->
