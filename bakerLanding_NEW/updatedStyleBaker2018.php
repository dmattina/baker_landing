<?php
/**
 * Template Name: New Discover Landing Page 2018 Style
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Here for the making</title>


	<style>.async-hide { opacity: 0 !important} </style>
	<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
	h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
	(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
	})(window,document.documentElement,'async-hide','dataLayer',4000,
	{'GTM-5QPVDP4':true});</script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5JDHH2');</script>
	<!-- End Google Tag Manager -->

<!-- Oracle Maxymiser Script Start -->
<script type="text/javascript” src=”//service.maxymiser.net/api/us/keypathedu.com/651afc/mmapi.js"></script>
<!-- Oracle Maxymiser Script End -->

<!-- BEGIN: Marin Software Tracking Script (Landing Page) -->
<script type='text/javascript'>
var _mTrack = _mTrack || [];
_mTrack.push(['trackPage']);

(function() {
var mClientId = '16680tk457738';
var mProto = (('https:' == document.location.protocol) ? 'https://' : 'http://');
var mHost = 'tracker.marinsm.com';

var mt = document.createElement('script'); mt.type = 'text/javascript'; mt.async = true; mt.src = mProto + mHost + '/tracker/async/' + mClientId + '.js';
var fscr = document.getElementsByTagName('script')[0]; fscr.parentNode.insertBefore(mt, fscr);
})();
</script>
<noscript>
<img width="1" height="1" src="https://tracker.marinsm.com/tp?act=1&cid=16680tk457738&script=no" />
</noscript>
<!-- END: Copyright Marin Software -->

<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory');?>/dist/css/styles.css">

<!-- <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory');?>/dist/css/stylesLandingV1.css"> -->
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/favicon-16x16.png">
<link rel="mask-icon" href="<?php echo get_bloginfo('template_directory');?>/assets/images/favicon/safari-pinned-tab.svg" color="#5bbad5">

</head>
	<body>
<div id="LandingPage">
		<!-- Google Tag Manager (nLandingPage" class="businessoscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5JDHH2"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div id="DiscoverLandingPage">

	<div class="main-header LanndingPage">
	<div class="row">
		<div class="main-header__container">
			<div class="main-header__search-bar">
				<input type="text" class="main-header__search-input" placeholder="Search">
				<div class="main-header__search-input-close"></div>
			</div>
			<div class="main-header__left-nav">
				<a href="/" class="main-header__logo">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/logo/new-logo.svg" alt="" class="main-header__logo-img-red">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/logo/new-logo--white.svg" alt="" class="main-header__logo-img-white">
				</a>
			</div>
			<div class="main-header__right-nav">
				<a class="main-header__right-btn main-header__call-btn" href="tel:810-766-2170"></a>
				<a class="main-header__call-text" href="tel:810-766-2170">810-766-2170</a>
			</div>
		</div>
	</div>
</div>

<div class="LandingPage-fullwidth-video-cta" style="background-image: url('<?php the_field('header_image'); ?>');  background-size: cover; background-position: center;">
<div class="row">
	<div class="LandingPage-fullwidth-video-cta__wrap" style="max-width:  40em;">
		<h1 class="LandingPage-fullwidth-video-cta__title"><?php the_field('header_title'); ?></h1>
		<div class="LandingPage-fullwidth-video-cta__sub-text">
			<p><?php the_field('header_copy'); ?></p>
		</div>
	</div>
	</div>
</div>
</div>
<div class="Content">
<div class="row">
	<div class="Content__Right lead-form keypath fixme" id="cform" >
		<div class="lead-form_head">
			<h3>Request Info</h3>
		</div>
		<?php
			if( ! is_page('grad-studies-online') ) {
				get_template_part('KeypathForm_traffic');
			}
			if( is_page('grad-studies-online') ) {
				get_template_part('keypathFormGradStudies');
			}
		?>
	</div>
	<div class="Content__Left">
		<div class="success_holder">

			<?php the_field('sucess_starts_here_para') ?>
	  </div>
 	<h3>Here for the <span class="red">making.</span></h3>

		<div class="locations-grid">
			<h5>For Your Convenience</h5>
			<h3 class="title">Flexible Campuses</h3>
			<div class="row">
				<div class="locations-grid__icons">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Online-Red.svg"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/OnSite-Red.svg">
				</div>
				<div class="locations-grid__location">
					<div class="locations-grid__location-name"><h3>Allen Park</h3></div>
					<div class="locations-grid__location-address">4500 Enterprise Drive<br>Allen Park, MI 48101</div>
				</div>
				<div class="locations-grid__icons">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Online-Red.svg"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/OnSite-Red.svg">
				</div>
				<div class="locations-grid__location">
					<div class="locations-grid__location-name"><h3>Auburn Hills</h3></div>
					<div class="locations-grid__location-address">1500 University Drive<br>Auburn Hills, MI 48326</div>
				</div>
			</div>
			<div class="row">
				<div class="locations-grid__icons">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Online-Red.svg"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/OnSite-Red.svg">
				</div>
				<div class="locations-grid__location">
					<div class="locations-grid__location-name"><h3>Cadillac</h3></div>
					<div class="locations-grid__location-address">9600 E. 13th Street<br>Cadillac, MI 49601</div>
				</div>
				<div class="locations-grid__icons">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Online-Red.svg"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/OnSite-Red.svg">
				</div>
				<div class="locations-grid__location">
					<div class="locations-grid__location-name"><h3>Clinton Township</h3></div>
					<div class="locations-grid__location-address">34950 Little Mack Avenue<br>Clinton Twp., MI 48035</div>
				</div>
			</div>
			<div class="row">
				<div class="locations-grid__icons">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Online-Red.svg"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/OnSite-Red.svg">
				</div>
				<div class="locations-grid__location">
					<div class="locations-grid__location-name"><h3>Flint</h3></div>
					<div class="locations-grid__location-address">1050 W. Bristol Road<br>Flint, MI 48507</div>
				</div>
				<div class="locations-grid__icons">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Online-Red.svg"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/OnSite-Red.svg">
				</div>
				<div class="locations-grid__location">
					<div class="locations-grid__location-name"><h3>Jackson</h3></div>
					<div class="locations-grid__location-address">2800 Springport Road<br>Jackson, MI 49202</div>
				</div>
			</div>
			<div class="row">
				<div class="locations-grid__icons">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Online-Red.svg"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/OnSite-Red.svg">
				</div>
				<div class="locations-grid__location">
					<div class="locations-grid__location-name"><h3>Muskegon</h3></div>
					<div class="locations-grid__location-address">1903 Marquette Avenue<br>Muskegon, MI 49442</div>
				</div>
				<div class="locations-grid__icons">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Online-Red.svg"><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/OnSite-Red.svg">
				</div>
				<div class="locations-grid__location">
					<div class="locations-grid__location-name"><h3>Owosso</h3></div>
					<div class="locations-grid__location-address">1020 S. Washington St.<br>Owosso, MI 48867</div>
				</div>
			</div>
		</div>


		<div class="locations-grid global">
			<h3 class="title">Global Campuses</h3>
			<div class="row">
				<div class="locations-grid__icons">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Online-Red.svg">
				</div>
				<div class="locations-grid__location">
					<div class="locations-grid__location-name"><h3>Center for Graduate Studies</h3></div>
					<div class="locations-grid__location-address">1116 W. Bristol Road<br>Flint, MI 48507</div>
				</div>
				<div class="locations-grid__icons">
					<img src="<?php echo get_bloginfo('template_directory');?>/assets/images/Online-Red.svg">
				</div>
				<div class="locations-grid__location">
					<div class="locations-grid__location-name"><h3>Baker Online</h3></div>
					<div class="locations-grid__location-address">1116 W. Bristol Road<br>Flint, MI 48507</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>


<div class="programs">
	<div class="row">
		<div class="programs-title">
		<h5>Explore Your Future</h5>
		<h2><?php the_field('degree_name'); ?></h2>
		</div>
		<?php if( get_field('how_may_columns_of_programs') == '1' ): ?>
		<div class="programs-Programs">
			<div class="center">
				<div class="Count">
					<h3 class="number"><?php the_field('degree_program_1_amount'); ?></h3>
					<h5 class="title"><?php the_field('degree_program_1_name'); ?></h5>
				</div>
				<div class="Programs">
					<h5>Program Sampling</h5>
					<?php the_field('degree_program_1_programs'); ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<?php if( get_field('how_may_columns_of_programs') == '2' ): ?>
		<div class="programs-Programs">
			<div class="left">
				<div class="Count">
					<h3 class="number"><?php the_field('degree_program_1_amount'); ?></h3>
					<h5 class="title"><?php the_field('degree_program_1_name'); ?></h5>
				</div>
				<div class="Programs">
					<h5>Program Sampling</h5>
					<?php the_field('degree_program_1_programs'); ?>
				</div>
			</div>
			<div class="right">
				<div class="Count">
					<h3 class="number"><?php the_field('degree_program_2_amount'); ?></h3>
					<h5 class="title"><?php the_field('degree_program_2_name'); ?></h5>
				</div>
				<div class="Programs">
					<h5>Program Sampling</h5>
						<?php the_field('degree_program_2_programs'); ?>
				</div>
		</div>
	</div>
	<?php endif; ?>
</div>
</div>

<div class="locations" style="background: none">
	<div class="row">
	<div class="Affiliated_Institutions">
		<div class="row">
			<h2>Affiliated Institutions</h2>
			<div class="Affiliated_Institutions__location">
				<img src="https://www.baker.edu/assets/images/affiliated-institution-landing/cim-logo-secondary-nav.svg" />
				<div class="contain cim">
					<div class="Affiliated_Institutions__location-name"><h3>Culinary Institute of Michigan</h3></div>
					<div class="Affiliated_Institutions__location-name"><h4>Muskegon</h4></div>
					<div class="Affiliated_Institutions__location-address">1903 Marquette Avenue<br>Muskegon, MI 49442</div>
				</div>
			</div>
			<div class="Affiliated_Institutions__location">
				<img src="https://www.baker.edu/assets/images/affiliated-institution-landing/cim-logo-secondary-nav.svg" />
				<div class="contain cim">
					<div class="Affiliated_Institutions__location-name"><h3>Culinary Institute of Michigan</h3></div>
					<div class="Affiliated_Institutions__location-name"><h4>Port Huron</h4></div>
					<div class="Affiliated_Institutions__location-address">3402 Lapper Road<br>Port Huron, MI 48060</div>
				</div>
			</div>
			<div class="Affiliated_Institutions__location">
				<img class="svg" src="<?php echo get_bloginfo('template_directory');?>/assets/images/logo/ADI_Logo.svg" />
				<div class="contain adi">
					<div class="Affiliated_Institutions__location-name"><h3>Auto/Diesel Institute of Michigan</h3></div>
					<div class="Affiliated_Institutions__location-name"><h4>Owosso</h4></div>
					<div class="Affiliated_Institutions__location-address">1309 S. M-52<br>Owosso, MI 48867</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>


<footer class="footer-section">
	<div class="footer-section__bottom row">
		<div class="footer-section__logo">
				<img class="logo-icon" src="https://www.baker.edu/assets/images/logo/new-logo--red-white.svg"/>
		</div>
		<p class="footer-section__accredited">Accredited by <span class="footer-section__link_underline"><a href="#">The Higher Learning Commission</a></span>. An  equal opportunity affirmative action institution.
		An approved institution of the <span class="footer-section__link_underline"><a href="#">National Council for State Authorization Reciprocity Agreements (NC-SARA)</a></span>
		and the <span class="footer-section__link_underline"><a  href="#">Midwestern Higher Education Compact (MHEC)</a></span>
		</p>
		<div class="footer-section__bottom_rights-reserved"><a href="http://www.baker.edu">Visit baker.edu</a>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;© 2017 All rights reserved. Baker College</div>
	</div>
</footer>

		<div class="fade-screen"></div>
		</div>
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5890dcbf83803e36"></script>
		<script src="<?php echo get_bloginfo('template_directory');?>/dist/js/main.js" type="text/javascript" /></script>
		<script src="<?php echo get_bloginfo('template_directory');?>/dist/js/zip.js" type="text/javascript" /></script>

		<!-- <script src="bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js" type="text/javascript"></script>
		<script src="<?php echo get_bloginfo('template_directory');?>/dist/js/jquery.validate.js" type="text/javascript"></script>
		<script src="<?php echo get_bloginfo('template_directory');?>/dist/js/additional-methods.js" type="text/javascript"></script> -->


		<script src="https://use.typekit.net/jyv0hxv.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
		<script>

		$.validator.addMethod('customphone', function (value, element) {
		  return this.optional(element) || /^\d{3}-\d{3}-\d{4}$/.test(value);
		}, "Please enter a valid phone number");

		$.validator.methods.email = function( value, element ) {
  	return this.optional( element ) || /[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+/.test( value );
		}

		$.validator.addMethod("fillProgram", function(value, element, param) {
		  return this.optional(element) || value != param;
		}, "Please specify the program you are interested in");

		$.validator.addMethod("zipValid", function(value, element) {
		  return this.optional(element) || /^\d{5}$/.test(value);
		}, "Please provide a valid zipcode.");

		$.validator.addMethod("nonNumeric", function(value, element) {
		  return this.optional(element) || !value.match(/[0-9]+/);
		},"Only alphabatic characters allowed.");

		Inputmask.extendDefaults({
		  'autoUnmask': true
		});

		$().ready(function() {
			// validate signup form on keyup and submit
			$(".keypath").validate({
				rules: {
					firstname: "required",
					lastname: "required",
					email: {
						required: true,
					},
					dayphone: "customphone",
					address: "required",
					city: {
						required: true,
						nonNumeric: true
					},
					state: "required",
					zip: "zipValid",
					gradyear: "required",
					LocationID: "required",
					CurriculumID: "required",
				},
				messages: {
					firstname: "Please enter your First Name",
					lastname: "Please enter your Last Name",
					email: "Please enter a valid email address",
					dayphone: "Please enter a valid phone number",
					address: "Please enter your Address",
					city: "Please enter your City",
					state: "Please enter your State",
					zip: "Please enter your Zip Code",
					gradyear: "Please enter your Graduation Year",
					LocationID: "Please enter your Preferred campus location",
					CurriculumID: "Please specify the program you are interested in",
				}
			});
		});
		$('.Programs > h2').append(' <i>  </i>');
		$('.Programs > h3').append(' <i>  </i>');
    $('.Programs > h3').click(function(e) {
        var current = $(this).next('blockquote');
        $('.Programs > blockquote').not(current).hide(100);
        $('.Programs > h3 i').not($(this).find('i')).removeClass('open_arrow');
        $('.Programs > h3').not($(this)).removeClass('open');
        current.slideToggle();
        if ($(this).find('i').hasClass('open_arrow') || $(this).hasClass('open')) {
            $(this).find('i').removeClass('open_arrow');
            $(this).removeClass('open');
        } else if (!$(this).find('i').hasClass('open_arrow') || !$(this).hasClass('open')) {
            $(this).find('i').addClass('open_arrow');
            $(this).addClass('open')
        }
        if ($(window).width() < 767) {}
    });

		//Follow form Js
		$(window).on('resize', function () {
		    $('.Thank .head').css('height', $(this).height() + "px");
		    formScroll();
		});

		function formScroll() {
			var fixmeTop = $('.fixme').offset().top;               // get initial position of the element
		  var fixmePosition = $('.fixme').position();
		  var leftmargin = $('.fixme').css('margin-left');
		  var formwidth = document.getElementById('cform').offsetWidth;
		  //var intoSize = $('.intro').height() + 35;
		  var intoSize = $('#intro').height();
			var windowSize = $(window).width();
			var leadFormHeight= $('.contact-form__form').outerHeight();
		  $('.fixme').css({marginTop: '-' + intoSize + 'px',})

		  var collapsePoint = $('.head').height() + 100    // Banner height + Form height
			console.log(collapsePoint);

		  //$(window).scroll(function() {
		  $(window).on('scroll touchmove', function(e) {                   // assign scroll event listener
		        var currentScroll = $(window).scrollTop();        // get current position
		        var windowSize = $(window).width();
		        var formHeight= $('.fixme').height();
		        /*var tilesHeight= $('.tiles').offset().top ;
		        var stopMe = tilesHeight - formHeight;*/
		        var tilesHeight= $('.programs').offset().top ;
		        var stopMe = tilesHeight - formHeight;
						var leadFormHeight= $('.contact-form__form').outerHeight();



		        	if (currentScroll >= (fixmeTop + 20) && windowSize > 1000) {    // apply position: fixed if you
		            var fromTop = $('.fixme').offset().top;
		            $('.fixme').css({                                 // scroll to that element or below it
				          position: 'fixed',
				          top: '0',
				          left: (fixmePosition.left) + 'px',
									width: formwidth,
									marginLeft: '0',
				          marginTop: '-' + intoSize + 'px',
				          });
				        }
			        else {                               // apply position: static
			        $('.fixme').css({                      // if you scroll above it
								position:'',
								left :'',
								top: '',
								marginTop:'',
								marginLeft: '',
								width:'',
			          });
			        }

							if (currentScroll >= (stopMe) && windowSize > 1000) {           // apply position: fixed if you
		              $('.fixme').css({                                 // scroll to that element or below it
		                  position: 'absolute',
		                  /*width: width + 'px',*/
		                  top: stopMe + 'px',
		                  left: fixmePosition.left + 'px',
		                  marginTop: '-' + intoSize + 'px',
		                });
			        	}
								if (windowSize < 991){
									$('.fixme').css({      // scroll to that element or below it
										left: '',
										margin: '0 -20px',
									 });


								}

				  	});
					if (windowSize < 991){
							 $( ".lead-form_head" ).unbind('click').click(function(){
								 if ($('.lead-form_head').hasClass('closed')) {
									$( '.fixme' ).animate({top: '0', bottom: '0'});
									$( '.lead-form_head' ).removeClass('closed');
									$( '.lead-form_head' ).addClass('open');
								}
								 else {
 									$( '.fixme' ).animate({ bottom: '-' + leadFormHeight + 'px' });
									$( '.fixme' ).css({ top:'' });
 									$( '.lead-form_head' ).removeClass('open');
 									$( '.lead-form_head' ).addClass('closed');
								}
 							});

						}
				//console.log (leftmargin);
				}


				$(document).ready(function(){
		    		formScroll();
				var windowSize = $(window).width();
				if (windowSize < 991){
				var leadFormHeight= $('.contact-form__form').outerHeight();
					$( '.lead-form_head' ).addClass('closed');
					$('.fixme').css({      // scroll to that element or below it
						position: 'fixed',
						/*width: width + 'px',*/
						top: 'unset',
						left: '',
						margin: '0 -20px',
						bottom: '-' + leadFormHeight + 'px',
					 });
					}
				});
				$(document).ready(function(){

				  /* Catch Query strings and pass the same to Home page Icons link - Start */
				  var queryParameters = window.location.href.split("?")[1];
				  if(queryParameters != null){
				    $(".Programs a").each(function(){
				      var temphref = $(this).attr("href");
				      temphref += "?";
				      temphref += queryParameters;
				      $(this).attr("href", temphref);
				    });
				  }
				});

		</script>

		</body>
</html>
