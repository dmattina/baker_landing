<?php
/**
 * Template Name: New Thank You Page Style
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Discover St. Francis Law</title>
	<link rel="shortcut icon" href="https://stfrancislaw.com/wp-content/themes/stfrancislaw/favicon.ico">

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-60795620-1', 'auto');
  ga('require', 'GTM-PNV2NW8');
  ga('send', 'pageview');
</script>

<style>.async-hide { opacity: 0 !important} </style>
<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
})(window,document.documentElement,'async-hide','dataLayer',4000,
{'GTM-PNV2NW8':true});</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PFLTQ4');</script>
<!-- End Google Tag Manager -->

<!-- Oracle Maxymiser Script Start -->
<script type="text/javascript" src="//service.maxymiser.net/api/us/keypathedu.com/651afc/mmapi.js"></script>
<!-- Oracle Maxymiser Script End -->


	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory');?>/dist/css/styles.css">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory');?>/dist/css/SFLanding.css">


</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PFLTQ4"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="LandingPage">
	<div class="LandingPage-fullwidth-video-cta" style="background-image: url('<?php the_field('header_image'); ?>');">
	<div class="main-header LanndingPage">
	<div class="row">
		<div class="main-header__container">
			<div class="main-header__search-bar">
				<input type="text" class="main-header__search-input" placeholder="Search">
				<div class="main-header__search-input-close"></div>
			</div>
			<div class="main-header__left-nav">
				<a href="/" class="main-header__logo">
					<img src="<?php echo get_bloginfo('template_directory');?>/slices/logo 2.png" alt="" class="main-header__logo-img-red">
					<img src="<?php echo get_bloginfo('template_directory');?>/slices/logo 2.png" alt="" class="main-header__logo-img-white">
				</a>
			</div>
			<div class="main-header__right-nav">
				<a class="main-header__right-btn main-header__call-btn" href="tel:800-931-2694"></a>
				<a class="main-header__call-text" href="tel:810-766-2170">(800) 931-2694</a>
			</div>
		</div>
	</div>
  </div>
	<div class="row">
		<div class="LandingPage-fullwidth-video-cta__wrap">
			<h1 class="LandingPage-fullwidth-video-cta__title">Justice for your future.</h1>
			<div class="LandingPage-fullwidth-video-cta__sub-text">
				<p>Complete a Juris Doctor program on your own terms. The St. Francis School of Law offers an innovative online J.D. program with classes led by practicing lawyers.</p>
			</div>
			<a href="#cform" class="LandingPage-fullwidth-video-cta__cta-btn">
				Get Started Today
			</a>
		</div>
	</div>
</div>

<div class="contact">
<div class="row">
    <div class="contact-form-head" id="cform">
	<h2> Thank you </h2>
	<p>One of our admissions team will be with you soon</p>
	</div>
</div>
</div>



<footer class="footer-section">
	<div class="footer-section__bottom row">
		<div class="footer-section__logo">
				<img class="logo-icon" src="<?php echo get_bloginfo('template_directory');?>/slices/logo 2.png"/>
		</div>
		<div class="footer-section__bottom_rights-reserved"><a href="https://stfrancislaw.com/">Visit stfrancislaw.com</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;© 2017 St. Francis School of Law. All Rights Reserved.</div>
	</div>
</footer>

		<div class="fade-screen"></div>
		</div>
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5890dcbf83803e36"></script>
		<script src="<?php echo get_bloginfo('template_directory');?>/dist/js/main.js" type="text/javascript" /></script>
		<script src="<?php echo get_bloginfo('template_directory');?>/dist/js/zip.js" type="text/javascript" /></script>

		<!-- <script src="bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js" type="text/javascript"></script>
		<script src="dist/js/jquery.validate.js" type="text/javascript"></script>
		<script src="dist/js/additional-methods.js" type="text/javascript"></script> -->


		<script src="https://use.typekit.net/jyv0hxv.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
		<script>

		$.validator.addMethod('customphone', function (value, element) {
		  return this.optional(element) || /^\d{3}-\d{3}-\d{4}$/.test(value);
		}, "Please enter a valid phone number");

		$.validator.methods.email = function( value, element ) {
  	return this.optional( element ) || /[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+/.test( value );
		}

		$.validator.addMethod("fillProgram", function(value, element, param) {
		  return this.optional(element) || value != param;
		}, "Please specify the program you are interested in");

		$.validator.addMethod("zipValid", function(value, element) {
		  return this.optional(element) || /^\d{5}$/.test(value);
		}, "Please provide a valid zipcode.");

		$.validator.addMethod("nonNumeric", function(value, element) {
		  return this.optional(element) || !value.match(/[0-9]+/);
		},"Only alphabatic characters allowed.");

		Inputmask.extendDefaults({
		  'autoUnmask': true
		});

		$().ready(function() {
			// validate signup form on keyup and submit
			$(".keypath").validate({
				rules: {
					firstname: "required",
					lastname: "required",
					email: {
						required: true,
					},
					dayphone: "customphone",
					address: "required",
					city: {
						required: true,
						nonNumeric: true
					},
					state: "required",
					zip: "zipValid",
					gradyear: "required",
					LocationID: "required",
					CurriculumID: "required",
				},
				messages: {
					firstname: "Please enter your First Name",
					lastname: "Please enter your Last Name",
					email: "Please enter a valid email address",
					dayphone: "Please enter a valid phone number",
					address: "Please enter your Address",
					city: "Please enter your City",
					state: "Please enter your State",
					zip: "Please enter your Zip Code",
					gradyear: "Please enter your Graduation Year",
					LocationID: "Please enter your Preferred campus location",
					CurriculumID: "Please specify the program you are interested in",
				}
			});
		});

		</script>

</body>
</html>

<?php get_footer(); ?>
