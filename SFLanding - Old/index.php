<?php
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<!-- test -->

<div class="head" style="background: <?php the_field('big_slide_image'); ?> ;">
<!-- Slide/video splash -->
  <header>
    <div id="logo">
      <img src="/wp-content/themes/bakerLanding/slices\Baker College Logo.png" alt="Baker_Logo">
    </div><!-- close logo-->
    <div id="SlideInfo">

<?php if( get_field('headline') ): ?>
      <h1><?php the_field($headline); ?></h1>
<?php endif; ?>

	<a href="<?php the_field('play_button_url_'); ?>" target="_blank"><img src="/wp-content/themes/bakerLanding/slices/Video Ply BTN.png" alt="Play Video"></a>
      <p><?php get_field('sub-headline') ?></p>
    </div><!-- close SlideInfo -->
  </header>
</div><!-- close head -->

<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
